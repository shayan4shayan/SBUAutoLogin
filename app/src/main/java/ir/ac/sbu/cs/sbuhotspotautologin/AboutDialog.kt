package ir.ac.sbu.cs.sbuhotspotautologin

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import kotlinx.android.synthetic.main.dialog_about.*

/**
 * Created by shayan4shayan on 1/27/18.
 * just displaying simple dialog
 */
class AboutDialog(context: Context) : AlertDialog(context) {
    override fun show() {
        super.show()
        setContentView(R.layout.dialog_about)
        telegram.setOnClickListener { openTelegram() }
        instagram.setOnClickListener { openInstagram() }
        cssbu.setOnClickListener { openWeb() }
        source.setOnClickListener { openGit() }
    }

    private fun openGit() {
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse("https://gitlab.com/shayan4shayan/SBUAutoLogin")
        context.startActivity(i)
    }

    private fun openWeb() {
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse("http://cs.sbu.ac.ir")
        context.startActivity(i)
    }

    private fun openInstagram() {
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse("https://instagram.com/cs.sbu/")
        context.startActivity(i)
    }

    private fun openTelegram() {
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse("https://t.me/cssbu")
        context.startActivity(i)
    }
}