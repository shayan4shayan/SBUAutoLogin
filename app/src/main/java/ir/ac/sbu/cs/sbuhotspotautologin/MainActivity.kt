package ir.ac.sbu.cs.sbuhotspotautologin

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.telecom.Call
import android.util.Log
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.content_main.*
import java.util.*

class MainActivity : AppCompatActivity(), AddDialog.Callback, View.OnClickListener, RemainedVolumeLoader.Callback {
    override fun onLoaded(remained: String) {
        val cleaned = remained.replace("UNITS", "").replace(",", "").trim()
        Log.d("Main", cleaned)
        val num = cleaned.toFloat()
        if (num >= 1024) {
            textRemained.text = String.format("%1.1f", num / 1024)
            type.text = "GB"
        } else {
            textRemained.text = String.format("%1f", num)
            type.text = "MB"
        }
    }

    /**
     * click listener of manual login button
     */
    override fun onClick(p0: View?) {
        startService(Intent(this, LoginService::class.java))
    }

    /**
     * adds a new Account to data base
     */
    override fun add(username: String, password: String) {
        account.deleteAll()
        account.insert(username, password)
        displayToast("$username set")
        updateUsername()
        updateVolume()
    }

    private fun updateUsername() {
        if (account.selectAll().size > 0) {
            val acc = account.selectAll()[0]
            text_username.text = acc.username
        } else {
            text_username.text = "No Account Set"
        }
    }

    /**
     * display Toast
     */
    private fun displayToast(s: String) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show()
    }

    /**
     * data base
     */
    lateinit var account: Account

    /**
     * creating app main view
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        account = Account(this)


        login.setOnClickListener(this)
        changeAccount.setOnClickListener { AddDialog(this, this).show() }
        about.setOnClickListener { about() }
        sbu.setOnClickListener { sbu() }
        food.setOnClickListener { food() }
        golestan.setOnClickListener { golestan() }

        updateUsername()

        updateVolume()


        registerReminder()
    }

    private fun registerReminder() {
        val cal = Calendar.getInstance()
        val day = cal.get(Calendar.DAY_OF_WEEK)

        val wed = Calendar.WEDNESDAY

        var diff = wed - day
        if (diff < 0) diff += 7


        val manager = getSystemService(Context.ALARM_SERVICE) as AlarmManager

        manager.cancel(getPendingIntent())

        manager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + getDayInMiliSec(diff), getDayInMiliSec(7).toLong(), getPendingIntent())
    }

    private fun getPendingIntent(): PendingIntent {
        val intent = Intent(this, Receiver::class.java)
        return PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
    }

    private fun getDayInMiliSec(day: Int) = if (day == 0) 60 else day * 24 * 60 * 60 * 1000

    fun sbu() {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse("http://sbu.ac.ir")
        startActivity(intent)
    }

    fun food() {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse("http://dining.sbu.ac.ir")
        startActivity(intent)
    }

    fun golestan() {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse("http://golestan.sbu.ac.ir")
        startActivity(intent)
    }

    private fun updateVolume() {
        val users = account.selectAll()
        if (users.isEmpty()) {
            return
        }
        val u = users[0]
        val loader = RemainedVolumeLoader(this, this)
        loader.startLoad(u.username, u.password)
    }

    /**
     *     displaying about dialog
     */
    private fun about(): Boolean {
        displayAboutDialog()
        return true
    }

    private fun displayAboutDialog() {
        AboutDialog(this).show()
    }
}
