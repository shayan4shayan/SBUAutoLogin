package ir.ac.sbu.cs.sbuhotspotautologin

import android.app.AlertDialog
import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.dialog_select_account.*

/**
 * Created by shayan4shayan on 1/27/18.
 * showing dialog containing list of accounts to user to select
 */
class SelectAccountDialog(context: Context, val callBack: Callback) : AlertDialog(context) {
    /**
     * init dialog view and displaying list
     */
    override fun show() {
        super.show()
        setContentView(R.layout.dialog_select_account)
        recycler.layoutManager = LinearLayoutManager(context)
        recycler.adapter = Adapter(context, object : DialogCallback {
            override fun onClick(user: Account.User) {
                callBack.onSelected(user)
                dismiss()
            }
        })
        recycler.adapter.notifyDataSetChanged()
    }

    /**
     * adapter class for show list of accounts
     */
    class Adapter(val context: Context, private val callBack: DialogCallback) : RecyclerView.Adapter<SelectAccountDialog.ViewHolder>() {
        override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
            holder?.text?.text = list?.get(position)?.username
        }

        override fun getItemCount(): Int {
            Log.d("Dialog", "${list?.size}")
            return list?.size!!
        }

        override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
            val holder = ViewHolder(
                    LayoutInflater.from(context).inflate(R.layout.text_view, parent, false)
            )
            holder.itemView.setOnClickListener({ callBack.onClick(list!![holder.adapterPosition]) })
            return holder
        }

        /**
         * list for store accounts in ram
         */
        private var list: List<Account.User>? = null

        init {
            //getting accounts from local data base
            list = Account(context).selectAll()
        }
    }

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        var text: TextView? = null

        init {
            text = itemView.findViewById(R.id.text) as TextView
        }
    }

    /**
     * call back for send account to activity
     */
    interface Callback {
        fun onSelected(user: Account.User)
    }

    /**
     * callback for send account to dialog
     */
    interface DialogCallback {
        fun onClick(user: Account.User)
    }
}