package ir.ac.sbu.cs.sbuhotspotautologin

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

/**
 * Created by shayan4shayan on 1/24/18.
 * data base for save account on device
 */
class Account(c: Context) : SQLiteOpenHelper(c, "Account", null, 1) {

    /**
     * name of column username
     */
    private val username = "username"
    /**
     * name of column password
     */
    private val password = "password"
    /**
     * name of column id
     */
    val id = "id"

    /**
     * creating table
     */
    override fun onCreate(p0: SQLiteDatabase?) {
        val sql = "CREATE TABLE  $databaseName  ( $id INTEGER PRIMARY KEY, $username TEXT, $password TEXT);"
        p0?.execSQL(sql)
    }

    /**
     * doing nothing
     */
    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {

    }

    /**
     * inserting new Account to database
     */
    fun insert(user: String?, pass: String?) {
        val values = ContentValues()
        values.put(username, user)
        values.put(password, pass)
        writableDatabase.insert(databaseName, null, values)
    }

    @SuppressLint("Recycle")
            /**
             * select all accounts from database
             */
    fun selectAll(): ArrayList<User> {
        val users = ArrayList<User>()

        val sql = "SELECT * FROM $databaseName"
        val cursor = readableDatabase.rawQuery(sql, null)
        if (!cursor.moveToFirst()) {
            return users
        }

        do {
            val user = User(cursor.getString(cursor.getColumnIndex(username)),
                    cursor.getString(cursor.getColumnIndex(password)),
                    cursor.getInt(cursor.getColumnIndex(id)))
            users.add(user)

        } while (cursor.moveToNext())
        return users
    }

    /**
     * delete an account from database
     */
    fun delete(user: Int) {
        val sql = "DELETE FROM $databaseName WHERE $id=$user ;"
        writableDatabase.execSQL(sql)
    }

    /**
     * model class for user
     */
    class User(val username: String, val password: String, val id: Int)

    fun deleteAll() {
        val sql = "DELETE FROM $databaseName WHERE 1 ;"
        writableDatabase.execSQL(sql)
    }
}