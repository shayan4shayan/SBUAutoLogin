package ir.ac.sbu.cs.sbuhotspotautologin

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView

/**
 * Created by shayan4shayan on 1/24/18.
 * adapter for main activity list
 */
class AccountAdapter(private val callBack: CallBack) : RecyclerView.Adapter<AccountAdapter.ViewHolder>() {
    /**
     * model for save accounts in ram
     */
    var list: ArrayList<Account.User>? = null

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val viewHolder = ViewHolder(
                LayoutInflater.from(parent?.context).inflate(R.layout.item, parent, false)
        )
        viewHolder.delete?.setOnClickListener {
            val acc = list?.get(viewHolder.adapterPosition)
            callBack.onDelete(acc)
        }
        return viewHolder
    }

    override fun getItemCount(): Int {
        return list?.size!!
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.text?.text = list!![position].username
    }

    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        var text: TextView? = null
        var delete: ImageView? = null

        init {
            text = itemView?.findViewById(R.id.text) as TextView
            delete = itemView?.findViewById(R.id.delete) as ImageView
        }
    }

    /**
     * callback for send delete action to activity
     */
    interface CallBack {
        fun onDelete(account: Account.User?)
    }
}